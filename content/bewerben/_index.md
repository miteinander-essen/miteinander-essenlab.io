---
title: Bewerben
breadcrumb: Bewerben
---

Mach **Werbung** für die Veranstaltung!
Sag es deiner Nachbarin/deinem Nachbar, Freund oder Freundin.
Teile unsere Flyer (
[Original](/miteinander-essen-flyer-orig.pdf),
[ohne Hintergrund](/miteinander-essen-flyer-ohne-hintergrund.pdf),
[schwarz-weiß](/miteinander-essen-flyer-sw.pdf)) in der **Nachbarschaft** aus.

Teile unsere Seite auf **Social Media**.
Verlinke "Miteinander Essen" auf [Instagram](https://www.instagram.com/miteinanderessen/) oder
[Facebook](https://www.facebook.com/miteinanderessenerh/).
Verwende unser [Sharepic](/images/sharepic.jpg).

#### Sharepic

[![Sharepic](/images/sharepic.jpg)](/images/sharepic.jpg)

#### Flyer

[![Flyer original](/images/flyer-preview-orig.jpg)](/miteinander-essen-flyer-orig.pdf)
[![Flyer ohne Hintergrund](/images/flyer-preview-ohne-hintergrund.png)](/miteinander-essen-flyer-ohne-hintergrund.pdf)
[![Flyer schwarz-weiß](/images/flyer-preview-sw.png)](/miteinander-essen-flyer-sw.pdf)

- [Original](/miteinander-essen-flyer-orig.pdf)
- [ohne Hintergrund](/miteinander-essen-flyer-ohne-hintergrund.pdf)
- [schwarz-weiß](/miteinander-essen-flyer-sw.pdf)
