---
title: Registrieren
breadcrumb: Registrieren
---

Bitte teile uns in der E-Mail, im Fax oder am Telefon Folgendes mit:

- deinen Namen
- ob du als junge oder ältere Person teilnehmen willst
- ob du zum Zeitpunkt des Treffens vollständig gegen Corona geimpft sein wirst oder kürzlich von Corona genesen bist (2-G-Regel)
- ob du am 12.09.2021 (Sonntag) oder am 17.09.2021 (Freitag) jeweils um 19:00 Uhr Zeit hast
- wie viele Personen ihr seid (maximal 3)
- deine Telefonnummer und E-Mail-Adresse für Rückfragen

Wenn du als **junge Person** teilnehmen willst, dann außerdem:

- die Adresse, an der du deine Gäste empfängst, oder, wenn du keinen geeigneten Ort hast, dann, dass du gerne bei deiner Partnerin / deinem Partner kochen willst

Wenn du als **ältere Person** teilnehmen willst, dann außerdem:

- etwaige Unverträglichkeiten oder Essenswünsche
